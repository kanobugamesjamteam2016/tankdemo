﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TankDemo
{
	public class MonstersManager
	{
		#region Variables

		// Constants
		private const int cMonstersPoolCount = 10;
		private const float cDelayToSpawnMonster = 1.0f;

		private const string cSkeletonPrefabPath = "Prefabs/Skeleton";
		private const string cTrollPrefabPath = "Prefabs/Troll";
		private const string cSpiderPrefabPath = "Prefabs/Spider";

		private readonly Dictionary<EMonsterType, string> _monsterPrefabPath = new Dictionary<EMonsterType, string>();

		private List<IMonster> _monstersPool = new List<IMonster>();
		private List<IMonster> _monstersDeactivated = new List<IMonster>();

		private MonstersManagerData _managerData;

		private float _spawnTimer;
		private IActor _monsterTarget;

		#endregion

		#region Interface

		public struct MonstersManagerData
		{
			public MonstersData MonstersData;
			public Transform[] SpawnPoints;
		}

		public MonstersManager(MonstersManagerData data)
		{
			_managerData = data;
			_monsterPrefabPath.Add(EMonsterType.Skeleton, cSkeletonPrefabPath);
			_monsterPrefabPath.Add(EMonsterType.Troll, cTrollPrefabPath);
			_monsterPrefabPath.Add(EMonsterType.Spider, cSpiderPrefabPath);
		}

		/// <summary>
		///		Use this for initialization
		/// </summary>
		public void Initialize()
		{
			_spawnTimer = 0.0f;
			CreateMonstersPool();
		}

		public void SetTarget(IActor target)
		{
			_monsterTarget = target;

			foreach (var monster in _monstersPool)
			{
				monster.SetActorToHunt(_monsterTarget);
			}
		}

		/// <summary>
		///		Update is called once per frame
		/// </summary>
		public void Update()
		{
			_spawnTimer += Time.deltaTime;

			if (_spawnTimer >= cDelayToSpawnMonster && _monstersPool.Count < cMonstersPoolCount)
			{
				_spawnTimer = 0.0f;
				SpawnMonster();
			}
		}

		public void DestroyMonsters()
		{
			foreach (var monster in _monstersPool)
			{
				monster.OnActorDeath -= OnMonsterDeath;
				GameObject.Destroy(monster.ActorTransform.gameObject);
			}

			foreach (var monster in _monstersDeactivated)
			{
				monster.OnActorDeath -= OnMonsterDeath;
				GameObject.Destroy(monster.ActorTransform.gameObject);
			}

			_monstersPool.Clear();
			_monstersDeactivated.Clear();
		}

		#endregion

		#region Implementation

		private void CreateMonstersPool()
		{
			_monstersDeactivated.Clear();

			for (int i = 0; i < _managerData.MonstersData.Monsters.Length; i++)
			{
				string prefabPath = _monsterPrefabPath[_managerData.MonstersData.Monsters[i]];
				GameObject prefab = Resources.Load<GameObject>(prefabPath);
				GameObject monster = GameObject.Instantiate(prefab);
				IMonster monsterComponent = monster.GetComponent<IMonster>();

				if (monsterComponent != null)
				{
					_monstersDeactivated.Add(monsterComponent);
					monsterComponent.Deactivate();
					monsterComponent.OnActorDeath += OnMonsterDeath;
				}
			}
		}

		private void SpawnMonster()
		{
			int rndSpawnPointIndex = UnityEngine.Random.Range(0, _managerData.SpawnPoints.Length);
			Transform spawnPoint = _managerData.SpawnPoints[rndSpawnPointIndex];

			int rndMonsterIndex = UnityEngine.Random.Range(0, _monstersDeactivated.Count);

			if (_monstersDeactivated.Count > 0)
			{
				IMonster monster = _monstersDeactivated[rndMonsterIndex];

				_monstersDeactivated.Remove(monster);
				_monstersPool.Add(monster);
				monster.Activate();
				monster.SetActorToHunt(_monsterTarget);
				monster.ActorTransform.position = spawnPoint.position;
			}
		}

		private void OnMonsterDeath(IMonster killedMonster)
		{
			if (killedMonster != null)
			{
				_monstersPool.Remove(killedMonster);
				killedMonster.Deactivate();
				_monstersDeactivated.Add(killedMonster);
				SpawnMonster();
			}
		}

		#endregion
	}
}