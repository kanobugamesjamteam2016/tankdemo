﻿using UnityEngine;
using System.Collections;

namespace TankDemo
{
	public class ScoreMenuState : StateActivator<ScoreMenuBehaviour>
	{
		#region Variables

		// Constants
		private const string cScoreTextFormat = "Killed monsters : {0}";

		private ISceneSwitcher _sceneSwitcher;

		#endregion

		#region Interface

		public ScoreMenuState(IServiceController serviceController) : base(serviceController)
		{
			_sceneSwitcher = serviceController.GetService<ISceneSwitcher>();
		}

		public override void OnSceneEnabled(params object[] args)
		{
			base.OnSceneEnabled(args);

			int score = 0;
			foreach (var argument in args)
			{
				if (argument is int)
				{
					score = (int)argument;
				}
			}

			SubscribeToUiEvents();
			SetScoreText(score);
		}

		public override void OnSceneDeactivated()
		{
			base.OnSceneDeactivated();
			UnsubscribeFromUiEvents();
		}

		#endregion

		#region Implementation()

		private void SubscribeToUiEvents()
		{
			SceneBehaviour.OnRestartButtonClickedEvent += OnRestartButtonClicked;
			SceneBehaviour.OnExitButtonClickedEvent += OnExitClicked;
		}

		private void UnsubscribeFromUiEvents()
		{
			SceneBehaviour.OnRestartButtonClickedEvent -= OnRestartButtonClicked;
			SceneBehaviour.OnExitButtonClickedEvent -= OnExitClicked;
		}

		private void OnRestartButtonClicked()
		{
			_sceneSwitcher.SetState<GameState>();
		}

		private void OnExitClicked()
		{
			Application.Quit();
		}

		private void SetScoreText(int score)
		{
			SceneBehaviour.ScoreText.text = string.Format(cScoreTextFormat, score);
		}

		#endregion
	}
}