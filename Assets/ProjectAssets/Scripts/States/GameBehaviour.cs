﻿using UnityEngine;
using System.Collections;

namespace TankDemo
{
	public class GameBehaviour : MonoBehaviour
	{
		#region Variables

		[SerializeField]
		private Transform[] _spawnPoints;
		[SerializeField]
		private Transform _tankSpawnPoint;
		[SerializeField]
		private MonstersData _monstersData;
		[SerializeField]
		private WeaponData _weaponData;
		[SerializeField]
		private Camera _mainCamera;

		#endregion

		#region Interface
		#endregion

		#region Properties

		public Transform[] SpawnPoints { get { return _spawnPoints; } }
		public Transform TankSpawnPoint { get { return _tankSpawnPoint; } }
		public MonstersData MonstersData { get { return _monstersData; } }
		public WeaponData WeaponData { get { return _weaponData; } }
		public Camera MainCamera { get { return _mainCamera; } }

		#endregion
	}
}