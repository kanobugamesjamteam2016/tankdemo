﻿using UnityEngine;
using System.Collections;

namespace TankDemo
{
	public class MainMenuState : StateActivator<MainMenuBehaviour>
	{
		#region Variables

		private ISceneSwitcher _sceneSwitcher;

		#endregion

		#region Interface

		public MainMenuState(IServiceController serviceController) : base(serviceController)
		{
			_sceneSwitcher = serviceController.GetService<ISceneSwitcher>();
		}

		public override void OnSceneEnabled(params object[] args)
		{
			base.OnSceneEnabled(args);
			SubscribeToUiEvents();
		}

		public override void OnSceneDeactivated()
		{
			base.OnSceneDeactivated();
			UnsubscribeFromUiEvents();
		}

		#endregion

		#region Implementation()

		private void SubscribeToUiEvents()
		{
			SceneBehaviour.OnStartButtonClickedEvent += OnStartButtonClicked;
		}

		private void UnsubscribeFromUiEvents()
		{
			SceneBehaviour.OnStartButtonClickedEvent -= OnStartButtonClicked;
		}

		private void OnStartButtonClicked()
		{
			_sceneSwitcher.SetState<GameState>();
		}

		#endregion
	}
}