﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace TankDemo
{
	public class ScoreMenuBehaviour : MonoBehaviour
	{
		#region Variables

		[SerializeField]
		private Text _scoreText;

		#endregion

		#region Interface

		public void OnRestartClicked()
		{
			if (OnRestartButtonClickedEvent != null)
			{
				OnRestartButtonClickedEvent();
			}
		}

		public void OnExitClicked()
		{
			if (OnExitButtonClickedEvent != null)
			{
				OnExitButtonClickedEvent();
			}
		}

		#endregion

		#region Properties

		public Text ScoreText { get { return _scoreText; } }
		public event Action OnRestartButtonClickedEvent;
		public event Action OnExitButtonClickedEvent;

		#endregion
	}
}