﻿using System.Collections;

namespace TankDemo
{
	public class GameState : StateActivator<GameBehaviour>
	{
		#region Variables

		private ISceneSwitcher _sceneSwitcher;
		private GameController _gameController;
		private IEnumerator _gameControllerUpdater;

		#endregion

		#region Interface

		public GameState(IServiceController serviceController) : base(serviceController)
		{
			_sceneSwitcher = serviceController.GetService<ISceneSwitcher>();
		}

		public override void OnSceneEnabled(params object[] args)
		{
			base.OnSceneEnabled(args);

			InitializeGameController();

			_gameController.StartGame();
			_gameControllerUpdater = UpdateGameController();
			SceneBehaviour.StartCoroutine(_gameControllerUpdater);
		}

		public override void OnSceneDeactivated()
		{
			base.OnSceneDeactivated();
			SceneBehaviour.StopCoroutine(_gameControllerUpdater);
			_gameController.OnGameOver -= OnGameOver;
			_gameController = null;
		}

		#endregion

		#region Implementation()

		private void InitializeGameController()
		{
			GameController.GameControllerData data = new GameController.GameControllerData
			{
				MainCamera = SceneBehaviour.MainCamera,
				MonstersData = SceneBehaviour.MonstersData,
				SpawnPoints = SceneBehaviour.SpawnPoints,
				TankSpawnPoint = SceneBehaviour.TankSpawnPoint,
				WeaponData = SceneBehaviour.WeaponData
			};

			_gameController = new GameController(data);
			_gameController.Initialize();
			_gameController.OnGameOver += OnGameOver;
		}

		private void OnGameOver(int score)
		{
			_sceneSwitcher.SetState<ScoreMenuState>(score);
		}

		private IEnumerator UpdateGameController()
		{
			while (true)
			{
				_gameController.Update();
				yield return 0;
			}
		}

		#endregion
	}
}