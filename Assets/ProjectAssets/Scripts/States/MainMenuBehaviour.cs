﻿using UnityEngine;
using System;

namespace TankDemo
{
	public class MainMenuBehaviour : MonoBehaviour
	{

		#region Variables
		#endregion

		#region Interface

		public void OnStartClicked()
		{
			if (OnStartButtonClickedEvent != null)
			{
				OnStartButtonClickedEvent();
			}
		}

		#endregion

		#region Properties

		public event Action OnStartButtonClickedEvent;

		#endregion
	}
}