﻿using UnityEngine;

namespace TankDemo
{
    public interface ISceneSwitcher : IService
	{
		void Initialize(Transform root, IServiceController serviceController);

		void SetState<T>(params object[] args) where T : BaseState;
    }
}