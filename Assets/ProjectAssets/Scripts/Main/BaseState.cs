﻿using UnityEngine;
using System.Collections.Generic;

namespace TankDemo
{
	public abstract class BaseState
	{
		#region Interface

		public BaseState(IServiceController serviceController)
		{
			ServiceController = serviceController;
		}

		public virtual void OnSceneEnabled(params object[] args)
		{

		}

		public virtual void OnSceneDeactivated()
		{

		}

		public abstract void SetSceneRoot(GameObject root);

		#endregion

		#region Properties

		public IServiceController ServiceController { get; set; }

		public GameObject SceneRoot { get; set; }

		#endregion
	}
}