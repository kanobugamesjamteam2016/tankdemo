﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;

namespace TankDemo
{
    public class SceneSwitcher : MonoBehaviour, ISceneSwitcher
    {
        #region Variables  

        private BaseState _currentState;

        private Dictionary<Type, BaseState> _statesMap = new Dictionary<Type, BaseState>();

        #endregion

        #region Interface

        public void Initialize(Transform root, IServiceController serviceController)
        {
            CreateStates(serviceController);
        }

        public void SetState<T>(params object[] args) where T : BaseState
        {
            if (_currentState != null)
            {
                _currentState.OnSceneDeactivated();
				_currentState.SceneRoot.gameObject.SetActive(false);
            }

            Type tempType = typeof(T);
            _currentState = _statesMap[tempType];
			_currentState.SceneRoot.gameObject.SetActive(true);
			_currentState.OnSceneEnabled(args);
		}

		#endregion

		#region Implementation

		private void CreateStates(IServiceController serviceController)
        {
            _statesMap.Add(typeof(ScoreMenuState), new ScoreMenuState(serviceController));
            _statesMap.Add(typeof(MainMenuState), new MainMenuState(serviceController));
            _statesMap.Add(typeof(GameState), new GameState(serviceController));
        }

        #endregion
    }
}