﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TankDemo
{
    public class ServiceController : IServiceController
    {
        #region Variables

        private const string cDebugMessagePrefixName = "[AppRoot] - ";
        private const string cDebugMessageFormat = "{0}{1}";
		
        private Dictionary<Type, IService> _services = new Dictionary<Type, IService>();

        private Transform _servicesContainer;

        #endregion

        #region Interface

        public ServiceController(Transform serviceRoot, Transform sceneRoot)
        {
            _servicesContainer = serviceRoot;
			SceneRoot = sceneRoot;
        }

        public T GetService<T>() where T : IService
        {
            Type type = typeof(T);
            if (!_services.ContainsKey(type))
            {
                Debug.LogWarning(string.Format(cDebugMessageFormat, cDebugMessagePrefixName, "Service with this type not registered."));
                return default(T);
            }

            foreach (object item in _services.Keys)
            {
                if (item == type)
                {
                    return (T)_services[type];
                }
            }

            return default(T);
        }

        public T AddService<T>(Type component) where T : IService
        {
            Type type = typeof(T);
            if (_services.ContainsKey(type))
            {
                Debug.LogWarning(string.Format(cDebugMessageFormat, cDebugMessagePrefixName, "Service with this type already registered."));
                return default(T);
            }

            GameObject newService = new GameObject(type.ToString());
            newService.transform.parent = _servicesContainer;
            Component behaviour = newService.AddComponent(component);
            _services.Add(type, (IService)behaviour);

            return (T)_services[type];
        }

		#endregion

		#region Properties

		public Transform SceneRoot { get; private set; }

		#endregion
	}
}