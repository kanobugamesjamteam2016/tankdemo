﻿using UnityEngine;
using System;

namespace TankDemo
{
	public abstract class StateActivator<T> : BaseState where T : MonoBehaviour
	{
		#region Variables

		#endregion

		#region Interface

		public StateActivator(IServiceController serviceController) : base(serviceController)
		{
			SceneBehaviour = serviceController.SceneRoot.GetComponentInChildren<T>(true);
			SceneRoot = SceneBehaviour.gameObject;
		}

		public override void SetSceneRoot(GameObject root)
		{
			SceneRoot = root;
			SceneBehaviour = root.GetComponent<T>();
		}

		#endregion

		#region Properties

		protected T SceneBehaviour { get; private set; }

		#endregion
	}
}