﻿using UnityEngine;
using System.Collections;

namespace TankDemo
{
	public class AppRoot : MonoBehaviour
	{
		#region Variables

		[SerializeField]
		private Transform _sceneRoot;
		[SerializeField]
		private Transform _servicesContainer;

		private ISceneSwitcher _sceneSwitcher;
		private IServiceController _serviceController;

		#endregion

		#region Interface
		#endregion

		#region Implementation

		private void Awake()
		{
			CreateServices();
			_sceneSwitcher.SetState<MainMenuState>();
		}
		
		private void CreateServices()
		{
			_serviceController = new ServiceController(_servicesContainer, _sceneRoot);

			_sceneSwitcher = _serviceController.AddService<ISceneSwitcher>(typeof(SceneSwitcher));

			_sceneSwitcher.Initialize(transform, _serviceController);
		}

		#endregion
	}
}