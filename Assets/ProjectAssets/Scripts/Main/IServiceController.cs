﻿using System;
using UnityEngine;

namespace TankDemo
{
	public interface IServiceController
	{
		T GetService<T>() where T : IService;

		T AddService<T>(Type component) where T : IService;

		Transform SceneRoot { get; }
	}
}