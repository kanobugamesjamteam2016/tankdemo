﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace TankDemo
{
	/// <summary>
	///		Base class for monster behaviour
	/// </summary>
	public abstract class MonsterBase : MonoBehaviour, IMonster
	{
		#region Variables

		// Constants
		private const string cAnimatorSpeedParameterName = "Speed";
		private const string cAnimatorAttackParameterName = "Attack";
		private const string cAnimatorDeathParameterName = "Death";
		private const float cDeathAnimationLength = 3.0f;
		private const float cLookRotationSpeed = 5.0f;

		[SerializeField]
		private NavMeshAgent _navMeshAgent;
		[SerializeField]
		private Animator _animator;
		[SerializeField]
		private Image _healthBar;
		[Header("Pawn characteristics")]
		[SerializeField]
		private float _healthValue = 20.0f;
		[SerializeField]
		private float _shieldValue = 0.95f;
		[SerializeField]
		private float _moveSpeed = 1.0f;
		[SerializeField]
		private float _damageValue = 5.0f;
		[SerializeField]
		private float _attackRange = 3.0f;
		[SerializeField]
		private float _attackDelay = 2.0f;

		private EMonsterState _currentMonsterState = EMonsterState.Idle;
		private IActor _targetActor;
		private float _attackTimer;

		#endregion

		#region Interface

		public void Activate()
		{
			gameObject.SetActive(true);
			ActorTransform = transform;
			
			_animator.ResetTrigger(cAnimatorAttackParameterName);
			_animator.ResetTrigger(cAnimatorDeathParameterName);

			OnActivated();

			Health = _healthValue;
			_healthBar.fillAmount = Health / _healthValue;
			_navMeshAgent.speed = _moveSpeed;
			_currentMonsterState = EMonsterState.Idle;
		}

		public void Deactivate()
		{
			gameObject.SetActive(false);
		}

		public void Hit(float damage)
		{
			if (_currentMonsterState == EMonsterState.Dead)
			{
				return;
			}

			Health -= damage * _shieldValue;

			_healthBar.fillAmount = Health / _healthValue;

			if (Health <= 0.0f)
			{
				_currentMonsterState = EMonsterState.Dead;
				Dead();
			}
		}

		public void SetActorToHunt(IActor actorToHunt)
		{
			_targetActor = actorToHunt;
			_currentMonsterState = EMonsterState.Walk;
		}

		protected virtual void Idle()
		{
			Animator.SetFloat(cAnimatorSpeedParameterName, 0.0f);
		}

		protected virtual void Walk()
		{
			if (_targetActor == null)
			{
				return;
			}

			Animator.SetFloat(cAnimatorSpeedParameterName, 1.0f);
			_navMeshAgent.SetDestination(_targetActor.ActorTransform.position);

			float distToTarget = Vector3.Distance(transform.position, _targetActor.ActorTransform.position);
			if (distToTarget <= _attackRange)
			{
				_currentMonsterState = EMonsterState.Attack;
			}
		}

		protected virtual void Attack()
		{
			if (_targetActor == null)
			{
				return;
			}

			_navMeshAgent.Stop();
			Animator.SetFloat(cAnimatorSpeedParameterName, 0.0f);

			_attackTimer += Time.deltaTime;

			if (_attackTimer > _attackDelay)
			{
				Animator.SetTrigger(cAnimatorAttackParameterName);
				_attackTimer = 0.0f;
				_targetActor.Hit(_damageValue);
			}

			float distToTarget = Vector3.Distance(transform.position, _targetActor.ActorTransform.position);
			if (distToTarget > _attackRange)
			{
				_currentMonsterState = EMonsterState.Walk;
				_navMeshAgent.Resume();
			}

			LookAtTarget(_targetActor.ActorTransform);
		}

		protected virtual void Dead()
		{
			_navMeshAgent.Stop();
			Animator.SetFloat(cAnimatorSpeedParameterName, 0.0f);
			Animator.SetTrigger(cAnimatorDeathParameterName);
			StartCoroutine(WaitForDeathAnimation());
		}

		protected virtual void OnActivated()
		{
			ActorTransform = transform;
		}

		#endregion

		#region Implementation

		private void Update()
		{
			switch (_currentMonsterState)
			{
				default:
				case EMonsterState.Idle:
					{
						Idle();
						break;
					}

				case EMonsterState.Walk:
					{
						Walk();
						break;
					}

				case EMonsterState.Attack:
					{
						Attack();
						break;
					}					
			}
		}

		private IEnumerator WaitForDeathAnimation()
		{
			yield return new WaitForSeconds(cDeathAnimationLength);

			if (OnActorDeath != null)
			{
				OnActorDeath(this);
			}
		}

		private void LookAtTarget(Transform target)
		{
			Vector3 lookDirection = target.position - ActorTransform.position;
			Quaternion rotation = Quaternion.LookRotation(lookDirection.normalized);
			rotation.z = 0;

			ActorTransform.rotation = Quaternion.Lerp(ActorTransform.rotation, rotation, Time.deltaTime * cLookRotationSpeed);
		}

		#endregion

		#region Properties

		protected NavMeshAgent NavMeshAgent { get { return _navMeshAgent; } }

		protected Animator Animator { get { return _animator; } }

		public Transform ActorTransform { get; private set; }

		public float Health
		{
			get; private set;
		}

		public event Action<IMonster> OnActorDeath;
		
		#endregion

		#region Types

		protected enum EMonsterState
		{
			Idle,
			Walk,
			Attack,
			Dead
		}

		#endregion
	}

}