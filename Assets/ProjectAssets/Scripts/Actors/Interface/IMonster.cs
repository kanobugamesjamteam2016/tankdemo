﻿using System;

namespace TankDemo
{
	public interface IMonster : IActor
	{
		void SetActorToHunt(IActor actorToHunt);

		event Action<IMonster> OnActorDeath;
	}
}