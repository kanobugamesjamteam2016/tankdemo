﻿using UnityEngine;
using System;

namespace TankDemo
{
	public interface IActor
	{
		void Activate();
		void Deactivate();
		void Hit(float damage);

		float Health { get; }
		Transform ActorTransform { get; }
	}
}