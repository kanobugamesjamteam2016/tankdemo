﻿using System;

namespace TankDemo
{
	public interface ITank : IActor
	{
		void Initialize(WeaponData weaponData);

		event Action OnMonsterKilled;
		event Action OnActorDeath;
	}
}