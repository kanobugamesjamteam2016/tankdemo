﻿using UnityEngine;
using System.Collections;

namespace TankDemo
{
	public class MonstersData : ScriptableObject
	{
		public EMonsterType[] Monsters;
	}

	public enum EMonsterType
	{
		Skeleton,
		Spider,
		Troll
	}
}