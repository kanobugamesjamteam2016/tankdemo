﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace TankDemo
{
	public class TankPawn : MonoBehaviour, ITank
	{
		#region Variables

		// Constants
		private const string cHorizontalAxisName = "Horizontal";
		private const string cVerticalAxisName = "Vertical";

		private const float cTankTopRotationSpeed = 10.0f;
		private const float cHealthValue = 100.0f;
		private const float cShieldValue = 0.1f;
		private const float cMaxMoveSpeed = 10.0f;
		private const float cMaxSteerAngle = 120.0f;

		[SerializeField]
		private Transform _tankTop;
		[SerializeField]
		private Transform _gunPoint;
		[SerializeField]
		private Rigidbody _rigidbody;
		[SerializeField]
		private Image _healthBar;

		private IWeaponController _weaponController;

		#endregion

		#region Interface

		public void Hit(float damage)
		{
			Health -= damage * Shield;

			_healthBar.fillAmount = Health / cHealthValue;

			if (Health <= 0.0f)
			{
				if (OnActorDeath != null)
				{
					OnActorDeath();
				}
			}
		}

		public void Initialize(WeaponData weaponData)
		{
			_weaponController = new WeaponController(weaponData, _gunPoint);
			_weaponController.InitializeWeapons();			
		}

		public void Activate()
		{
			Health = cHealthValue;
			Shield = cShieldValue;
			MoveSpeed = cMaxMoveSpeed;
			_healthBar.fillAmount = Health / cHealthValue;
			gameObject.SetActive(true);
		}

		public void Deactivate()
		{
			gameObject.SetActive(false);
		}

		#endregion

		#region Implementation

		// Update is called once per frame
		private void Update()
		{
			UpdateWeapon();
			UpdateTankTop();
		}

		private void FixedUpdate()
		{
			UpdateMotor();
		}

		private void UpdateMotor()
		{
			Vector3 movePosition = ActorTransform.forward * cMaxMoveSpeed * Input.GetAxis(cVerticalAxisName) * Time.deltaTime;
			float turn = cMaxSteerAngle * Input.GetAxis(cHorizontalAxisName) * Time.deltaTime;
			Quaternion rotation = Quaternion.Euler(0, turn, 0);

			_rigidbody.MovePosition(_rigidbody.position + movePosition);
			_rigidbody.MoveRotation(_rigidbody.rotation * rotation);
		}

		private void UpdateWeapon()
		{
			if (Input.GetKeyDown(KeyCode.Q))
			{
				_weaponController.PreviousWeapon();
			}

			if (Input.GetKeyDown(KeyCode.E))
			{
				_weaponController.NextWeapon();
			}

			if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.X))
			{
				if (_weaponController.Shoot())
				{
					if (OnMonsterKilled != null)
					{
						OnMonsterKilled();
					}
				}
			}
		}
		
		private void UpdateTankTop()
		{
			Vector3 mousePos = Input.mousePosition;
			mousePos.z = Vector3.Distance(_tankTop.position, Camera.main.transform.position);
			mousePos = Camera.main.ScreenToWorldPoint(mousePos);
			mousePos.y = _tankTop.position.y;
			
			Vector3 lookDirection = mousePos - _tankTop.position;

			Quaternion desiredRotation = Quaternion.LookRotation(lookDirection.normalized);
			_tankTop.rotation = Quaternion.Lerp(_tankTop.rotation, desiredRotation, Time.deltaTime * cTankTopRotationSpeed);
		}

		#endregion

		#region Properties

		public Transform ActorTransform
		{
			get
			{
				return transform;
			}
		}

		public float Health
		{
			get; private set;
		}

		public float MoveSpeed
		{
			get;
			private set;
		}

		public float Shield
		{
			get;
			private set;
		}

		public event Action OnActorDeath;
		public event Action OnMonsterKilled;

		#endregion
	}
}