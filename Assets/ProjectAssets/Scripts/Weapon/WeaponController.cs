﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace TankDemo
{
	public class WeaponController : IWeaponController
{
		#region Variables

		// Constants
		private const string cWeaponFireHoleTransformName = "FireHole";

		private readonly Dictionary<WeaponInfo, WeaponComponents> _weapons = new Dictionary<WeaponInfo, WeaponComponents>();

		private WeaponInfo _currentWeapon;
		private WeaponData _weaponData;

		private int _currentWeaponIndex;
		private Transform _gunHolder;

		private float _lastShootTime;

		#endregion

		#region Interface

		public WeaponController(WeaponData weaponData, Transform gunHolder)
		{
			_weaponData = weaponData;
			_gunHolder = gunHolder;
		}

		public void InitializeWeapons()
		{
			foreach (var info in _weaponData.WeaponInfos)
			{
				GameObject weaponObject = GameObject.Instantiate(info.Prefab);
				weaponObject.transform.parent = _gunHolder;
				weaponObject.transform.localPosition = Vector3.zero;
				weaponObject.transform.localEulerAngles = Vector3.zero;
				weaponObject.transform.localScale = Vector3.one;
				weaponObject.SetActive(false);


				Transform fireHole = weaponObject.transform.Find(cWeaponFireHoleTransformName);
				IWeaponFx weaponFx = fireHole.GetComponent<IWeaponFx>();

				WeaponComponents components = new WeaponComponents
				{
					WeaponObject = weaponObject,
					FireHole = fireHole,
					WeaponFx = weaponFx
				};

				_weapons.Add(info, components);
			}

			_currentWeaponIndex = 0;
			_currentWeapon = _weaponData.WeaponInfos[_currentWeaponIndex];
			_weapons[_currentWeapon].WeaponObject.SetActive(true);
		}

		public void NextWeapon()
		{
			_currentWeaponIndex++;

			if (_currentWeaponIndex >= _weaponData.WeaponInfos.Length)
			{
				_currentWeaponIndex = 0;
			}

			SetupCurrentWeapon();
		}

		public void PreviousWeapon()
		{
			_currentWeaponIndex--;

			if (_currentWeaponIndex < 0)
			{
				_currentWeaponIndex = _weaponData.WeaponInfos.Length - 1;
			}

			SetupCurrentWeapon();
		}

		public bool Shoot()
		{
			if (_currentWeapon.ShootDelay > (Time.time - _lastShootTime))
			{
				return false;
			}
			
			IActor actorToHit = GetHittedActor();

			if (actorToHit != null)
			{
				actorToHit.Hit(_currentWeapon.Damage);
				_lastShootTime = Time.time;
				return actorToHit.Health <= 0;
			}

			_lastShootTime = Time.time;
			return false;
		}

		#endregion

		#region Implementation

		private IActor GetHittedActor()
		{
			_currentWeapon = _weaponData.WeaponInfos[_currentWeaponIndex];

			WeaponComponents component = _weapons[_currentWeapon];

			component.WeaponFx.PlayFx();

			RaycastHit hit;

			if (Physics.Raycast(component.FireHole.position, component.FireHole.forward, out hit, Mathf.Infinity))
			{
				return hit.transform.GetComponent<IActor>();
			}

			return null;
		}

		private void SetupCurrentWeapon()
		{
			_weapons[_currentWeapon].WeaponFx.ClearFxParticles();
			_weapons[_currentWeapon].WeaponObject.SetActive(false);
			_currentWeapon = _weaponData.WeaponInfos[_currentWeaponIndex];
			_weapons[_currentWeapon].WeaponObject.SetActive(true);
		}

		#endregion

		#region Properties

		#endregion

		private struct WeaponComponents
		{
			public GameObject WeaponObject;
			public Transform FireHole;
			public IWeaponFx WeaponFx;
		}
	}
}