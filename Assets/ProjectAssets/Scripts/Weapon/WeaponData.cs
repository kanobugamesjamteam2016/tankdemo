﻿using UnityEngine;

namespace TankDemo
{
	public class WeaponData : ScriptableObject
	{
		public WeaponInfo[] WeaponInfos;
	}
}