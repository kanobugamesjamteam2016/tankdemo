﻿using UnityEngine;
using System;

namespace TankDemo
{
	[Serializable]
	public struct WeaponInfo
	{
		#region Variables
		
		[SerializeField]
		private GameObject _prefab;
		[SerializeField]
		private int _damage;
		[SerializeField]
		private float _shootDelay;
		[SerializeField]
		private string _name;
		[SerializeField]
		private string _prefabPath;
		
		#endregion

		#region Properties

		public GameObject Prefab { get { return _prefab; } }
		public int Damage { get { return _damage; } }
		public float ShootDelay { get { return _shootDelay; } }
		public string Name { get { return _name; } }
		public string PrefabPath { get { return _prefabPath; } }

		#endregion
	}
}