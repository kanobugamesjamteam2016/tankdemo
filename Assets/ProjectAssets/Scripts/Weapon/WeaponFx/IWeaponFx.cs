﻿using UnityEngine;
using System.Collections;

namespace TankDemo
{
	public interface IWeaponFx
	{
		void PlayFx();
		void ClearFxParticles();
	}
}