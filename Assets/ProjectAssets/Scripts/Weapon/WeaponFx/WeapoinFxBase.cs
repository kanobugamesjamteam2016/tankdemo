﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TankDemo
{
	public class WeapoinFxBase : MonoBehaviour, IWeaponFx
	{
		#region Variables

		// Constants
		private const float cParticleLifetime = 1.0f;

		[SerializeField]
		private GameObject _fxParticle;

		private List<GameObject> _instantiatedFx = new List<GameObject>();

		#endregion

		#region Interface

		public void PlayFx()
		{
			GameObject prefab = GameObject.Instantiate(_fxParticle, transform.position, transform.rotation) as GameObject;
			StartCoroutine(DestroyFx(prefab));
			_instantiatedFx.Add(prefab);
		}

		public void ClearFxParticles()
		{
			foreach (var fxPrefab in _instantiatedFx)
			{
				GameObject.Destroy(fxPrefab);
			}

			_instantiatedFx.Clear();
		}

		#endregion

		#region Implementation

		private IEnumerator DestroyFx(GameObject particlePrefab)
		{
			yield return new WaitForSeconds(cParticleLifetime);
			Destroy(particlePrefab);
		}

		#endregion
	}
}