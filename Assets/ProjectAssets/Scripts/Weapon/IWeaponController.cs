﻿namespace TankDemo
{
	public interface IWeaponController
	{
		void InitializeWeapons();
		void NextWeapon();
		void PreviousWeapon();
		bool Shoot();
	}
}