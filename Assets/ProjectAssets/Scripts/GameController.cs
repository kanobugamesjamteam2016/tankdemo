﻿using UnityEngine;
using System;

namespace TankDemo
{
	public class GameController
	{
		#region Variables

		private const string cTankPawnPath = "Prefabs/Tank";

		private Transform[] _spawnPoints;
		private Transform _tankSpawnPoint;
		private MonstersData _monstersData;
		private WeaponData _weaponData;
		private Camera _mainCamera;

		private MonstersManager _monsterManager;
		private ITank _tankPawn;
		private ICameraController _cameraController;
		private int _score;

		#endregion

		#region Interface

		public struct GameControllerData
		{
			public Transform[] SpawnPoints;
			public Transform TankSpawnPoint;
			public MonstersData MonstersData;
			public WeaponData WeaponData;
			public Camera MainCamera;
		}

		public GameController(GameControllerData data)
		{
			_spawnPoints = data.SpawnPoints;
			_tankSpawnPoint = data.TankSpawnPoint;
			_monstersData = data.MonstersData;
			_weaponData = data.WeaponData;
			_mainCamera = data.MainCamera;
		}

		/// <summary>
		///		Use this for initialization
		/// </summary>
		public void Initialize()
		{
			InitializeTankPawn();
			InitializeMonsterManager();
			InitializeGameCamera();
		}

		public void StartGame()
		{
			_score = 0;
			_tankPawn.Activate();
		}

		/// <summary>
		///		Update
		/// </summary>
		public void Update()
		{
			if (_monsterManager != null)
			{
				_monsterManager.Update();
			}

			if (_cameraController != null)
			{
				_cameraController.Update();
			}
		}

		#endregion

		#region Implementation

		private void InitializeMonsterManager()
		{
			MonstersManager.MonstersManagerData managerData = new MonstersManager.MonstersManagerData
			{
				SpawnPoints = _spawnPoints,
				MonstersData = _monstersData
			};

			_monsterManager = new MonstersManager(managerData);
			_monsterManager.SetTarget(_tankPawn);
			_monsterManager.Initialize();
		}

		private void InitializeTankPawn()
		{
			GameObject tankPrefab = Resources.Load<GameObject>(cTankPawnPath);
			if (tankPrefab != null)
			{
				GameObject tank = GameObject.Instantiate(tankPrefab);
				_tankPawn = tank.GetComponent<ITank>();
				_tankPawn.Initialize(_weaponData);
				_tankPawn.OnActorDeath += OnTankDeath;
				_tankPawn.OnMonsterKilled += OnMonsterKilled;
				tank.transform.position = _tankSpawnPoint.position;
			}
		}

		private void InitializeGameCamera()
		{
			_cameraController = new CameraController(_tankPawn.ActorTransform, _mainCamera);
		}

		private void OnTankDeath()
		{
			if (OnGameOver != null)
			{
				OnGameOver(_score);
			}

			_tankPawn.OnActorDeath -= OnTankDeath;
			_tankPawn.OnMonsterKilled -= OnMonsterKilled;
			_tankPawn.Deactivate();
			GameObject.Destroy(_tankPawn.ActorTransform.gameObject);
			_tankPawn = null;

			_cameraController = null;

			_monsterManager.DestroyMonsters();
			_monsterManager = null;
		}

		private void OnMonsterKilled()
		{
			_score++;
		}

		#endregion

		#region Properties

		public event Action<int> OnGameOver;
		#endregion
	}
}