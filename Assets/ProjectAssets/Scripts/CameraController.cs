﻿using UnityEngine;
using System.Collections;

namespace TankDemo
{
	public class CameraController : ICameraController
	{
		#region Variables

		// Constants
		private const float cCameraMoveSpeed = 1.0f;
		private const float cCameraRotationSpeed = 0.75f;
		private readonly Vector3 _cameraOffset = new Vector3(0.0f, 25.0f, 0.0f);
		private readonly Vector3 _cameraLookOffset = new Vector3(0.0f, 0.0f, 2.0f);

		private Camera _mainCamera;
		private Transform _transformToFollow;

		#endregion

		#region Interface

		public CameraController(Transform transformToFollow, Camera mainCamera)
		{
			_transformToFollow = transformToFollow;
			_mainCamera = mainCamera;
		}

		/// <summary>
		///  Update is called once per frame
		/// </summary>
		public void Update()
		{
			UpdateCameraPosition();
		}

		#endregion

		#region Implementation

		private void UpdateCameraPosition()
		{
			if (_transformToFollow == null)
			{
				return;
			}

			Vector3 nextPos = _transformToFollow.position;
			nextPos = nextPos + Vector3.forward * _cameraOffset.z;
			nextPos = nextPos + Vector3.up * _cameraOffset.y;
			nextPos = nextPos + Vector3.right * _cameraOffset.x;

			_mainCamera.transform.position = Vector3.Lerp(_mainCamera.transform.position, nextPos, Time.deltaTime * cCameraMoveSpeed);

			Vector3 lookPos = _transformToFollow.position;
			lookPos = lookPos + Vector3.forward * _cameraLookOffset.z;
			lookPos = lookPos + Vector3.up * _cameraLookOffset.y;
			lookPos = lookPos + Vector3.right * _cameraLookOffset.x;

			Vector3 cameraLookDirection = lookPos - _mainCamera.transform.position;
			Quaternion lookRotation = Quaternion.LookRotation(cameraLookDirection);

			_mainCamera.transform.rotation = Quaternion.Lerp(_mainCamera.transform.rotation, lookRotation, Time.deltaTime * cCameraRotationSpeed);
		}
		
		#endregion

		#region Properties
		#endregion

	}
}