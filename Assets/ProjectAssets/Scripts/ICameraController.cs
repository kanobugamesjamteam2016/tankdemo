﻿using UnityEngine;
using System.Collections;

namespace TankDemo
{
	public interface ICameraController
	{
		void Update();
	}
}