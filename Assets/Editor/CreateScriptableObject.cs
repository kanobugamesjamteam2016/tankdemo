﻿using UnityEngine;
using UnityEditor;

namespace TankDemo
{
	public class CreateScriptableObject : Editor
	{
		[MenuItem("Assets/ScriptableObject/WeaponData")]
		public static void CreateWeaponData()
		{
			CreateObjectAsset<WeaponData>("Assets/ProjectAssets/ScriptableObjects/NewWeaponData.asset");
		}

		[MenuItem("Assets/ScriptableObject/MonstersData")]
		public static void CreateMonstersData()
		{
			CreateObjectAsset<MonstersData>("Assets/ProjectAssets/ScriptableObjects/NewMonstersData.asset");
		}

		private static void CreateObjectAsset<T>(string assetPath) where T : ScriptableObject
		{
			T newInstance = ScriptableObject.CreateInstance<T>();

			AssetDatabase.CreateAsset(newInstance, assetPath);
			AssetDatabase.SaveAssets();

			EditorUtility.FocusProjectWindow();

			Selection.activeObject = newInstance;
		}
	}
}